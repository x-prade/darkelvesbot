from GameBot import db


def write_npc_to_base(name, ident, maxhp, lvl, pvp, minecrafter, required_minecrafter, damage, defense, miss, crit,
                      add_exp, add_aden, current_exp, required_exp, aden, donate,
                      ares=None, poseydon=None, gefest=None, zeus=None, kronos=None):
    npc_coll = db["npc"]
    new_npc = {"name": name,
               "indet": ident,
               "max_hp": maxhp,
               "current_hp": maxhp,
               "lvl": lvl,
               "pvp": pvp,
               "minecrafter": minecrafter,
               "required_minecrafter": required_minecrafter,
               "damage": damage,
               "defense": defense,
               "miss": miss,
               "crit": crit,
               "add_exp": add_exp,
               "add_aden": add_aden,
               "current_exp": current_exp,
               "required_exp": required_exp,
               "aden": aden,
               "donate": donate,
               "is_npc": False}
    if ares:
        new_npc.update({"ares": ares})
    if poseydon:
        new_npc.update({"poseydon": poseydon})
    if gefest:
        new_npc.update({"gefest": gefest})
    if zeus:
        new_npc.update({"zeus": zeus})
    if kronos:
        new_npc.update({"kronos": kronos})

    npc_coll.save(new_npc)
